import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TernaryPembayaran {
    public static void main(String[] args) throws IOException {
        int sksPerSemester = 24;
        int biayaPerSKS = 50000;
        int dendaPerSKS = 3000;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Masukkan jumlah mata kuliah yang Anda ambil: ");
        int jumlahSKS = Integer.parseInt(reader.readLine());

        int totalBiayaKuliah = jumlahSKS * biayaPerSKS;
        int biayaDenda = (jumlahSKS > sksPerSemester) ? ((jumlahSKS - sksPerSemester) * dendaPerSKS) : 0;
        int totalBiayaTermasukDenda = totalBiayaKuliah + biayaDenda;

        System.out.println("Total biaya kuliah Anda (termasuk denda): Rp " + totalBiayaTermasukDenda);
    }
}
